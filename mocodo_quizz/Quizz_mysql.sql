CREATE DATABASE IF NOT EXISTS `QUIZZ` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `QUIZZ`;

CREATE TABLE `REPONSE` (
  `coderesponse` VARCHAR(42),
  `textereponse` VARCHAR(42),
  `codequestion` VARCHAR(42),
  PRIMARY KEY (`coderesponse`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*
CREATE TABLE `DATE_PARTICIPATION` (
  `datedate` VARCHAR(42),
  PRIMARY KEY (`datedate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
*/

CREATE TABLE `QUIZZ` (
  `codequizz` VARCHAR(42),
  `nomquizz` VARCHAR(42),
  PRIMARY KEY (`codequizz`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `PARTICIPE` (
  `codemembre` VARCHAR(42),
  `codequizz` VARCHAR(42),
  `datedate` VARCHAR(42),
  `score` VARCHAR(42),
  PRIMARY KEY (`codemembre`, `codequizz`, `datedate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `QUESTION` (
  `codequestion` VARCHAR(42),
  `ordrequestion` VARCHAR(42),
  `textequestion` VARCHAR(42),
  `codebonnereponse` VARCHAR(42),
  `codequizz` VARCHAR(42),
  PRIMARY KEY (`codequestion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `MEMBRE` (
  `codemembre` VARCHAR(42),
  `nommembre` VARCHAR(42),
  `passmembre` VARCHAR(42),
  PRIMARY KEY (`codemembre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `REPONSE` ADD FOREIGN KEY (`codequestion`) REFERENCES `QUESTION` (`codequestion`);
-- ALTER TABLE `PARTICIPE` ADD FOREIGN KEY (`datedate`) REFERENCES `DATE_PARTICIPATION` (`datedate`);
ALTER TABLE `PARTICIPE` ADD FOREIGN KEY (`codequizz`) REFERENCES `QUIZZ` (`codequizz`);
ALTER TABLE `PARTICIPE` ADD FOREIGN KEY (`codemembre`) REFERENCES `MEMBRE` (`codemembre`);
ALTER TABLE `QUESTION` ADD FOREIGN KEY (`codequizz`) REFERENCES `QUIZZ` (`codequizz`);