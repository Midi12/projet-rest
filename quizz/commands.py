from .app import db
from flask_script import Command
from .models import TableDate, TableQuizz, TableMember, TableAnswer, TableQuestion, TableParticipate
from datetime import datetime

class createdb(Command):
	"Create empty db"

	def run(self):
		db.create_all()

		# insert creation date
		tdate = TableDate()
		tdate._date = datetime.now()
		print(tdate.getDate())
		db.session.add(tdate)
		db.session.commit()

		print("Empty db created")
