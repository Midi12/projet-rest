import os.path
def mkpath(p):
	return os.path.normpath(os.path.join(os.path.dirname(__file__), p))

from flask import Flask
app = Flask(__name__)
app.debug = True

#from flask.ext.sqlalchemy import SQLAlchemy
from flask_sqlalchemy import SQLAlchemy
app.config['SQLALCHEMY_DATABASE_URI'] = ('sqlite:///' + mkpath('../quizz.db'))
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)

app.config['SECRET_KEY'] = "975a6655-6958-472b-bed7-321a12a9d921"

from .commands import createdb
#from flask.ext.script import Manager
from flask_script import Manager
manager = Manager(app)
manager.add_command('createdb', createdb())

#from flask.ext.bootstrap import Bootstrap
from flask_bootstrap import Bootstrap
app.config['BOOTSTRAP_SERVE_LOCAL'] = True
Bootstrap(app)

#from flask.ext.login import LoginManager
from flask_login import LoginManager
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"
login_manager.login_message = "Veuillez vous authentifier."
login_manager.login_message_category = "info"
