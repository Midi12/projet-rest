from .app import app, db, login_manager
from flask import render_template, request
from .models import TableDate, TableQuizz, TableMember, TableAnswer, TableQuestion, TableParticipate
from flask import jsonify, abort
from flask_login import login_user , logout_user , current_user , login_required
from sqlalchemy import desc
import base64
from datetime import datetime

# home route, default page with samples
@app.route("/")
def home():
	return render_template(
		"frontpage.html",
		title="Quizzizy"
	)

'''
	*---------------*---------------*
	| HTTP Method   | Action	    |
	*---------------*---------------*
	|    GET        | Obtain info   |
	|               | about res     |
	*---------------*---------------*
	|    GET        | Obtain info   |
	|               | about res     |
	*---------------*---------------*
	|    POST       | create a new  |
	|               | resource      |
	*---------------*---------------*
	|    PUT        |  Update a     |
	|               |  resource     |
	*---------------*---------------*
	|    DELETE     | delete a      |
	|               | resource      |
	*---------------*---------------*
'''

@app.route('/quizz', methods=['GET'])
@app.route('/quizzs', methods=['GET'])
def get_quizzs():
	tq = TableQuizz.query.all()

	if (tq == None or tq == []):
		abort(404)
		return

	json_data = []
	for q in tq:
		json_data.append((q.getCode(), q.getName()))

	return jsonify({'quizzs' : json_data})

@app.route('/quizz/<int:qid>', methods=['GET'])
def get_quizz(qid):
	tq = TableQuizz.query.filter_by(_code = qid).first()

	if (tq == None or tq == []):
		abort(404)
		return

	questions = TableQuestion.query.filter_by(_codequizz = qid)
	questions_data = {}

	def get_order(value):
		return value.getOrder()

	for q in sorted(questions, key=get_order):
		answers = TableAnswer.query.filter_by(_codequestion = q.getCode())
		answers_data = []

		def get_code(value):
			return value.getCode()

		for a in sorted(answers, key=get_code):
			answers_data.append(a.getText())

		questions_data['q' + str(q.getCode())] = (q.getText(), q.getGoodIndex(), answers_data)


	json_data = {
		'quizz_id' : tq.getCode(),
		'quizz_name' : tq.getName(),
		'quizz_questions' : questions_data
	}

	return jsonify(json_data)

@app.route('/quizz/create', methods=['POST'])
def create_quizz():
	content = request.get_json()
	print(content)
	quizz = TableQuizz(_name=content["quizz_name"])
	db.session.add(quizz)
	db.session.commit()
	for question in content["quizz_questions"]:
		q = TableQuestion(
			_order=int(question[1:]),
			_text=content["quizz_questions"][question][0],
			_goodindex=content["quizz_questions"][question][1],
			_codequizz=quizz.getCode()
		)
		db.session.add(q)
		db.session.commit()
		for answer in content["quizz_questions"][question][2]:
			a = TableAnswer(
				_text=answer,
				_codequestion=q.getCode()
			)
			db.session.add(a)
			db.session.commit()
	return jsonify({"id":quizz.getCode()})

@app.route('/quizz/add', methods=['POST'])
def add_question():
	return

@app.route('/quizz/delete/<int:qid>/<int:qqid>', methods=['DELETE'])
def delete_question(qid, qqid):
	return

@app.route('/quizz/finish', methods=['POST'])
@login_required
def finish_quizz():
	content = request.get_json()
	print(content)

	user_ = content["user"]
	pts_ = content["pts"]
	qid_ = content["qz"]

	tp = TableParticipate()

	tp._codeMember = TableMember.query.filter_by(_name = user_).first().getCode()
	tp._codequizz = qid_
	tp._dateDate = datetime.now()
	tp._score = pts_

	db.session.add(tp)
	db.session.commit()

	return jsonify({'success': 'Bravo !'})

@app.route('/quizz/delete/<int:qid>', methods=['DELETE'])
def delete_quizz():
	return

@app.route('/quizz/update', methods=['PUT'])
def update_quizz():
	return

@app.errorhandler(404)
def not_found(e):
	return jsonify({'qerror' : 404,
					'message' : 'Resource not found'})

@app.errorhandler(403)
def forbidden(e):
	return jsonify({'qerror' : 403,
					'message' : 'Forbidden'})

@app.errorhandler(405)
def not_allowed(e):
	return jsonify({'qerror' : 405,
					'message' : 'Method not allowed'})

@app.route("/login", methods=['POST'])
def login():
	content = request.get_json(silent=True)

	username = content['username']
	password = content['password']

	registered_user = TableMember.query.filter_by(_name = username, _pass = password).first()
	if registered_user is None or not registered_user:
		return jsonify({'qerror': 1000,
						'message' : 'User credentials are wrong'})

	if not login_user(registered_user):
		return jsonify({'qerror': 1000,
						'message' : 'Cannot login user'})

	return jsonify({'success': 'Successfully logged in'})

@app.route("/logout", methods=['POST'])
@login_required
def logout():
	if not logout_user():
		return jsonify({'qerror': 1000,
						'message' : 'Cannot logout user'})

	return jsonify({'success': 'Successfully logged out'})

@app.route("/register", methods=['POST'])
def register():
	content = request.get_json(silent=True)

	username = content['username']
	password = content['password']

	registered_user = TableMember.query.filter_by(_name = username).first()
	if registered_user is not None:
		return jsonify({'qerror': 1000,
						'message' : 'User already registered'})

	new_user = TableMember()

	tmp_memb = TableMember.query.order_by(desc(TableMember._code)).first()

	new_user._code = 1 if tmp_memb is None else tmp_memb.getCode() + 1
	new_user._name = username
	new_user._pass = password

	db.session.add(new_user)
	db.session.commit()
	return jsonify({'success': 'Successfully registered in'})

@login_manager.unauthorized_handler
def unauthorized():
	return jsonify({'qerror' : 403,
					'message' : 'Unauthorized'})

@login_manager.user_loader
def load_user(user_id):
	return TableMember.query.filter_by(_name = user_id).first()

@login_manager.header_loader
def load_user_from_header(header_val):
	header_val = header_val.replace('Basic ', '', 1)
	user_id = user_pwd = ""

	try:
		header_val = base64.b64decode(header_val)
		print(header_val)
		s = str(header_val)
		sa = s.split("'")
		sa2 = sa[1].split(":")
		user_id = sa2[0]
		user_pwd = sa2[1]
	except TypeError:
		print("err")
		pass
	print(user_id + " " + user_pwd)
	return TableMember.query.filter_by(_name = user_id, _pass = user_pwd).first()
