from .app import db
import flask
import os
from sqlalchemy_utils import *
from sqlalchemy import *
from sqlalchemy.orm import relationship
from datetime import datetime

'''
class: TableQuizz
purpose: represent quizz informations
'''
class TableQuizz(db.Model):
	__tablename__ = 'Quizz'
	_code = db.Column(db.Integer, primary_key = True)
	_name = db.Column(db.String(100))

	def __repr__(self):
		return 'TableQuizz(' + _code + ', ' + _name + ')'

	def getCode(self):
		return self._code

	def getName(self):
		return self._name

'''
class: TableQuestion
purpose: represent question informations
'''
class TableQuestion(db.Model):
	__tablename__ = 'Question'
	_code = db.Column(db.Integer, primary_key = True)
	_order = db.Column(db.Integer)
	_text = db.Column(db.String(100))
	_goodindex = db.Column(db.Integer)
	_codequizz = db.Column(db.Integer, db.ForeignKey('Quizz._code'))
	_quizz = db.relationship('TableQuizz', backref=db.backref('Question'))

	def __repr__(self):
		return 'TableQuestion(' + _code + ', ' + _order + ', ' + _text + ', ' + _goodindex + ')'

	def getCode(self):
		return self._code

	def getOrder(self):
		return self._order

	def getText(self):
		return self._text

	def getGoodIndex(self):
		return self._goodindex

	def getQuizz(self):
		return self._quizz

'''
class: TableMember
purpose: represent member informations
'''
class TableMember(db.Model):
	__tablename__ = 'Member'
	_code = db.Column(db.Integer, primary_key = True)
	_name =  db.Column(db.String(15), unique = True, nullable = False)
	_pass = db.Column(db.String(15), unique = False, nullable = False)

	def __repr__(self):
		return 'TableMember(' + _code + ', ' + _name + ', ' + _pass + ')'

	def get_id(self):
		return str(self._name)

	def is_authenticated(self):
		return True

	def is_active(self):
		return True

	def is_anonymous(self):
		return False

	def get_id(self):
		return self._name

	def getCode(self):
		return self._code

	def getName(self):
		return self._name

	def getPass(self):
		return self._pass

'''
class: TableScore
purpose: represent date informations
'''
class TableDate(db.Model):
	__tablename__ = 'Date'
	_date = db.Column(db.DateTime, primary_key = True)

	def __repr__(self):
		return 'TableDate(' + _date + ')'

	def getDate(self):
		return self._date

'''
class: TableAnswer
purpose: represent answer informations
'''
class TableAnswer(db.Model):
	__tablename__ = 'Answer'
	_code = db.Column(db.Integer, primary_key = True)
	_text = db.Column(db.String(100))
	_codequestion = db.Column(db.Integer, db.ForeignKey('Question._code'))
	_question = db.relationship('TableQuestion', backref=db.backref('Answer'))

	def __repr__(self):
		return 'TableAnswer(' + _code + ', ' + _text + ')'

	def getCode(self):
		return self._code

	def getText(self):
		return self._text

	def getQuestion(self):
		return self._question

'''
class: TableParticipate
purpose: represent answer informations
'''
class TableParticipate(db.Model):
	__tablename__ = 'Participate'
	_codeMember = db.Column(db.Integer, db.ForeignKey('Member._code'))
	_member = db.relationship('TableMember', backref=db.backref('Participate'))
	_codequizz = db.Column(db.Integer, db.ForeignKey('Quizz._code'))
	_quizz = db.relationship('TableQuizz', backref=db.backref('Participate'))
	_dateDate = db.Column(db.DateTime, primary_key = True)
	_score = db.Column(db.Integer)
