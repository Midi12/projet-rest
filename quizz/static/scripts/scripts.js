$(window).load(function(){
	display_main_panel();
	reload_quizz_list();
})

function display_main_panel()
{
	$('.quizzizy-main-panel-title').html('Bienvenue sur Quizzizy');
	$('.quizzizy-main-panel-body').html('Choisissez une action');
	select_list_item(0);
}

function reload_quizz_list()
{
	$(".quizz-item").remove();
	$.ajax({
		type: "GET",
		url: "/quizz",
		error: function(msg) {
			styled_alert("error", "Erreur de chargement");
		},
		success: function(data) {
			for (var line in data.quizzs)
				$('#side-list').append('<a href="#" class="list-group-item quizz-item"'
				+ 'id="list-item-' + data.quizzs[line][0] + '"'
				+ ' onclick="display_quizz(' + data.quizzs[line][0] + ')">'
				+ '<span class="glyphicon glyphicon-play-circle"></span> '
				+ data.quizzs[line][1] + '</div>');
	}});
}

function select_list_item(i)
{
	$('.list-group-item').each(function(){
		$(this).removeClass("active");
	});
	$('#list-item-' + i).addClass("active");
}

function display_quizz(quizzid)
{
	select_list_item(quizzid);
	$.ajax({
		type: "GET",
		url: "/quizz/" + quizzid,
		error: function(msg) {
			styled_alert("error", "Erreur de chargement");
		},
		success: function(data) {
			$('.quizzizy-main-panel-title').html(data.quizz_name);
			$('.quizzizy-main-panel-body').html("");
			for (var question in data.quizz_questions)
			{
				var qid = question.charAt(1);
				$('.quizzizy-main-panel-body').append('<fieldset class="form-group quizzizy-question" id="form-group-' + qid + '"></fieldset>');
				$('#form-group-' + qid).append('<div class="panel panel-default" id="panel-' + qid + '">'
					+ '<div class="panel-heading" id="panel-heading-">'
					+ data.quizz_questions[question][0]
					+ '</div></div>');
				var cpt = 1;
				for (var answer in data.quizz_questions[question][2])
					$('#panel-' + qid).append('<div class="list-group-item"><div class="radio">'
					+ '<label><input type="radio" value=\"' + cpt++ + '\" name="answer-' + qid + '">' + data.quizz_questions[question][2][answer] + '</label>'
					+ '</div></div>');

			}
			$('.quizzizy-main-panel-body').append('<button type="button" class="btn btn-success" id="submit-quizz-btn" onclick="submit_quizz(' + quizzid + ');">Envoyer</button>');
	}});
}

function contains(a, obj) {
	for (var i = 0; i < a.length; i++) {
		if (a[i] === obj) {
			return true;
		}
	}
	return false;
}

function submit_quizz(qid)
{
	if (!g_logged)
	{
		show_login_panel();
		return;
	}

	var _data = null;
	$.ajax({
		type: "GET",
		url: "/quizz/" + qid,
		async: false,
		cache: false,
		timeout: 10000,
		error: function(msg)
		{
			styled_alert("error", "Error on getting questions data");
		},
		success: function(data)
		{
			_data = data;
		}
	});

	if (_data == null)
	{
		styled_alert("error", "Cannot get validation data");
		return;
	}

	var pts = 0;
	var nq = 0;

	//var b = null;

	for (var question in _data.quizz_questions)
	{
		var b = $('input[name="answer-' + question.charAt(1) + '"]:checked').val();

		if (b !== undefined)
			if (_data.quizz_questions[question][1] == parseInt(b))
				pts++;
		nq++;
	}

	$.ajax({
		type: "POST",
		url: "/quizz/finish",
		data: JSON.stringify({
				"user" : g_username,
				"pts" : pts,
				"qz" : qid
			}),
		error: function(msg)
		{
			styled_alert("error", "Error sending data");
			styled_alert("error", msg.statusText);
		},
		success: function(data)
		{

			styled_alert("success", "You got " + pts + "/" + nq + " points !");
		},
		dataType: "json",
		contentType: "application/json"
	});
}

function save_quizz()
{
	var json_quizz = {};
	var put_strs = [];
	json_quizz["quizz_name"] = $("#quizz-name").val();
	put_strs.push($("#quizz-name").val());
	var json_questions = {};
	var quit = false;
	$('.quizzizy-question-field').each(function() {
		var qid = $(this).attr("id").split("-")[1];
		json_questions["q" + qid] = [$(this).val()];
		put_strs.push($(this).val());
		var v = $('input[name="question-' + qid + '"]:checked').val();
		if (v === undefined)
		{
			styled_alert("error", "Veuillez selectionner la bonne réponse à la question " + qid);
			quit = true;
			return;
		}
		v = parseInt(v);
		json_questions["q" + qid].push(v);
		var answers = [];
		$('.quizzizy-answer-field-' + qid).each(function() {
			answers.push($(this).val());
			put_strs.push($(this).val());
		});
		json_questions["q" + qid].push(answers);
	});
	if (quit)
		return;
	if (contains(put_strs, ""))
	{
		styled_alert("error", "Il reste des champs vides !");
		return;
	}
	json_quizz["quizz_questions"] = json_questions;
	console.log(JSON.stringify(json_quizz));
	$.ajax({
		type: "POST",
		url: "quizz/create",
		dataType: "json",
		contentType: "application/json",
		data: JSON.stringify(json_quizz),
		error: function(msg) {
			styled_alert("error", "Erreur de création");
		},
		success: function(data) {
			styled_alert("success", "Quizz créé !");
			display_main_panel();
			reload_quizz_list();
	}});
}

function display_quizz_creation()
{
	select_list_item(0);
	$('.quizzizy-main-panel-title').html('<div class="input-group">'
	+'<input type="text" class="form-control" id="quizz-name" placeholder="Nom du quizz">'
	+ '<a href="#" onclick="display_main_panel()" class="input-group-addon" title="Annuler la création du quizz">'
	+'<span class="glyphicon glyphicon-remove"></span></a></div>');
	$('.quizzizy-main-panel-body').html('<button type="button" class="btn btn-default" id="add-question-btn" onclick="display_question_creation(1)">'
	+ 'Ajouter une question</button>');
	$('.quizzizy-main-panel-body').append('<button type="button" class="btn btn-default pull-right" id="save-quizz" onclick="save_quizz()">'
	+ 'Enregistrer le quizz</button>');
}

function display_question_creation(qid)
{
	$('#add-question-btn').attr('onclick', 'display_question_creation(' + (qid+1) + ')');
	$('#add-question-btn').before('<fieldset class="form-group" id="form-group-' + qid + '"></fieldset>');
	$('#form-group-' + qid).append('<div class="panel panel-default" id="panel-' + qid
	+ '"><div class="panel-heading" id="panel-heading-' + qid
	+ '"></div><div class="panel-body" id="panel-body-' + qid
	+ '"></div></div>');
	$('#panel-heading-' + qid).append('<div class="input-group">'
	+ '<input type="text" class="form-control quizzizy-question-field" id="question-' + qid + '" placeholder="Question ' + qid + '">'
	+ '<a href="#" onclick="remove_question(' + qid + ')" class="input-group-addon" title="Supprimer la question">'
	+ '<span class="glyphicon glyphicon-remove"></span></a></div>');
	$('#panel-body-' + qid).append('<button type="button" class="btn btn-default" id="add-answer-btn-' + qid
	+ '" onclick="display_answer_creation(' + qid + ', 1)">Ajouter une réponse</button>');
}

function display_answer_creation(qid, aid)
{
	$('#add-answer-btn-' + qid).attr('onclick', 'display_answer_creation(' + qid + ',' + (aid+1) + ')');
	$('#add-answer-btn-' + qid).before('<div class="input-group" id="input-group-' + qid + '-' + aid + '">'
	+ '<span class="input-group-addon"><input type="radio" name="question-'
	+ qid + '" value="' + aid + '"></span>'
	+ '<input type="text" class="form-control quizzizy-answer-field-' + qid
	+ '" id="answer-' + qid + '-' + aid + '" placeholder="Réponse ' + aid + '">'
	+ '<a href="#" onclick="remove_answer(' + qid + ', ' + aid + ')" class="input-group-addon" title="Supprimer la réponse">'
	+ '<span class="glyphicon glyphicon-remove"></span></a></div>');
}

function remove_question(qid)
{
	$('#form-group-' + qid).remove();
}

function remove_answer(qid, aid)
{
	$('#input-group-' + qid + '-' + aid).remove();
}

var g_login_toggle = false;
var g_logged = false;
var g_username = "";

function show_login_panel()
{
	var login_panel = 	!g_logged ? "<div id=\"loginPanel\" class=\"container\">" +
						"<div class=\"row\">" +
						"<div class=\"col-md-4 col-md-offset-4\">" +
						"<div class=\"panel panel-default\">" +
						"<div id=\"signup\"class=\"panel-body\">" +
						"<h5 class=\"text-center\">" +
						"SIGN UP</h5>" +
						"<form class=\"form form-signup\" role=\"form\">" +
						"<div class=\"form-group\">" +
						"<div class=\"input-group\">" +
						"<span class=\"input-group-addon\"><span class=\"glyphicon glyphicon-user\"></span></span>" +
						"<input id=\"username-register\" name=\"username\" type=\"text\" class=\"form-control\" placeholder=\"Username\" />" +
						"</div>" +
						"</div>" +
						"<div class=\"form-group\">" +
						"<div class=\"input-group\">" +
						"<span class=\"input-group-addon\"><span class=\"glyphicon glyphicon-envelope\"></span>" +
						"</span>" +
						"<input id=\"email-register\" name=\"email\" type=\"text\" class=\"form-control\" placeholder=\"Email Address\" />" +
						"</div>" +
						"</div>" +
						"<div class=\"form-group\">" +
						"<div class=\"input-group\">" +
						"<span class=\"input-group-addon\"><span class=\"glyphicon glyphicon-lock\"></span></span>" +
						"<input id=\"password-register\" name=\"password\" type=\"password\" class=\"form-control\" placeholder=\"Password\" />" +
						"</div>" +
						"</div>" +
						"</div>" +
						"<a href=\"#\" class=\"btn btn-sm btn-primary btn-block\" role=\"button\" onclick=\"register_user();\">" +
						"SUBMIT</a> </form>" +
						"<div class=\"quizzizy-spacer\"></div>"+
						"<div class=\"panel-body\">" +
						"<h5 class=\"text-center\">" +
						"LOGIN</h5>" +
						"<form class=\"form form-login\" role=\"form\">" +
						"<div class=\"form-group\">" +
						"<div class=\"input-group\">" +
						"<span class=\"input-group-addon\"><span class=\"glyphicon glyphicon-user\"></span></span>" +
						"<input id=\"username-login\" name=\"username\" type=\"text\" class=\"form-control\" placeholder=\"Username\" />" +
						"</div>" +
						"</div>" +
						"<div class=\"form-group\">" +
						"</div>" +
						"<div class=\"form-group\">" +
						"<div class=\"input-group\">" +
						"<span class=\"input-group-addon\"><span class=\"glyphicon glyphicon-lock\"></span></span>" +
						"<input id=\"password-login\" name=\"password\" type=\"password\" class=\"form-control\" placeholder=\"Password\" />" +
						"</div>" +
						"</div>" +
						"</div>" +
						"<a href=\"#\" class=\"btn btn-sm btn-primary btn-block\" role=\"button\" onclick=\"login_user();\">" +
						"SUBMIT</a> </form>" +
						"</div>" +
						"</div>" +
						"</div>" +
						"</div>" : "<div id=\"loginPanel\" class=\"label label-success\"><h1 id=\"usr\">" + g_username + "</h1><a href=\"#\" class=\"badge badge-success\" onclick=\"logout_user();\">Logout</a></div>";

	if (!g_login_toggle)
		$("#userButton").after(login_panel);
	else
		$("#loginPanel").remove();

	g_login_toggle = !g_login_toggle;
}

function login_user()
{
	$.ajax({
		type: "POST",
		url: "/login",
		data: JSON.stringify(
			{
				"username": $('#username-login').val(),
				"password": $('#password-login').val()
			}
		),
		error: function(msg)
		{
			if(!response_is_error(msg))
				styled_alert("error", msg.statusText);
		},
		success: function(data)
		{
			if (!response_is_error(data))
			{
				styled_alert("success", data.success);
				g_username = $('#username-login').val();
				g_logged = true;
				show_login_panel();
			}
		},
		dataType: "json",
		contentType: "application/json"
	});
}

function logout_user()
{
	$.ajax({
		type: "POST",
		url: "/logout",
		error: function(msg)
		{
			if(!response_is_error(msg))
				styled_alert("error", msg.statusText);
		},
		success: function(data)
		{
			if (!response_is_error(data))
			{
				styled_alert("success", data.success);
				g_username = "";
				g_logged = false;
				show_login_panel();
			}
		},
		dataType: "json",
		contentType: "application/json"
	});
}

function register_user()
{
	$.ajax({
		type: "POST",
		url: "/register",
		data: JSON.stringify(
			{
				"username": $('#username-register').val(),
				"password": $('#password-register').val()
			}
		),
		error: function(msg)
		{
			if(!response_is_error(msg))
				styled_alert("error", msg.statusText);
		},
		success: function(data)
		{
			if (!response_is_error(data))
			{
				styled_alert("success", data.success);
				show_login_panel();
			}
		},
		dataType: "json",
		contentType: "application/json"
	});
}

/* util functions*/
function response_is_error(data)
{
	if (data.hasOwnProperty('qerror'))
	{
		styled_alert('error', data.message);
		return true;
	}

	return false;
}

function styled_alert(lvl, msg)
{
	var style_prefix = "alert ";
	var style_suffix = " fade in quizzizy-flash-container-elem";
	var style = "";

	switch (lvl) {
		case "success":
			style += "alert-success";
			break;
		case "info":
			style += "alert-info";
			break;
		case "warning":
			style += "alert-warning";
			break;
		case "error":
			style += "alert-danger";
			break;
		default:
			style += "alert-inverse";
			break;
	}

	$('#flash-list').append('<li><div class=\"' + style_prefix + style + style_suffix + '\"><a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>' + msg + '</div></li> ');
}
